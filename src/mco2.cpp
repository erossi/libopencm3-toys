/* Copyright (C) 2021 Enrico Rossi
 * GPL 3

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*! \file
 *
 * On a Maple Mini
 */

#include <stdio.h>
#include <errno.h>
#include <cstring>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include "tick.h"

static void clock_setup(void)
{
	// high-speed external oscillator (HSE) at 8MHz.
	rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);

	// PA8-AF MCO
	rcc_periph_clock_enable(RCC_GPIOA);

	// PB0 Led
	rcc_periph_clock_enable(RCC_GPIOB);

  // Alternate Functions
  rcc_periph_clock_enable(RCC_AFIO);
}

static void gpio_setup(void)
{
	// Led PB1
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ,
			GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);

  // Enable GPIO A8-AF output MCO 50Mhz
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO8);
}

void _delay_ms(const uint32_t ms)
{
  const uint64_t i { _millis + ms };

  while (_millis < i )
    asm("nop");
}

int main() {
	clock_setup();
	systick_setup();
	gpio_setup();

	// Output MCO
	rcc_set_mco(RCC_CFGR_MCO_PLL_DIV2);

	// Toggle the LED on and off forever
	while (1) {
		gpio_toggle(GPIOB, GPIO1);	/* LED on/off */
		_delay_ms(1000);
	}

	return 0;
}
