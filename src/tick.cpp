/* Copyright (C) 2020, 2021 Enrico Rossi
 * LGPL 3

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this library; If not, see <http://www.gnu.org/licenses/>.
*/

/*! \file
 *
 * The System Tick timer is part of the ARM Cortex core. It is a 24 bit down
 * counter that can be configured with an automatical reload value.
 */

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>

#include "tick.h"

// Global definition
volatile uint64_t _millis;

// This is our interrupt handler for the systick reload interrupt.
// The full list of interrupt services routines that can be implemented is
// listed in libopencm3/include/libopencm3/stm32/f0/nvic.h
void sys_tick_handler() {
    _millis++;
}

void systick_setup() {
	// Set the systick clock source to our main clock
	// 72MHz / 8 => 9000000 counts/sec
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB_DIV8);

	// In order to trigger an interrupt every millisecond:
	// 9000000c/s / 9000 = 1000c/s overflows per second - every 1ms one interrupt.
	// SysTick interrupt every N clock pulses: set reload to N-1
	systick_set_reload(9000 - 1);

	// Clear the Current Value Register so that we start at 0
	STK_CVR = 0;

	// Enable interrupts from the system tick clock
	systick_interrupt_enable();

	// Enable the system tick counter
	systick_counter_enable();
}
