/* Copyright (C) 2021 Enrico Rossi
 * LGPL 3

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this library; If not, see <http://www.gnu.org/licenses/>.
*/

/*! \file
 */

#ifndef _TICK_H_
#define _TICK_H_

// Global declaration _millis
extern volatile uint64_t _millis;

void systick_setup();

#endif
