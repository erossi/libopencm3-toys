/** @brief <b>Test adc and dma.</b>
 *
 * @author @htmlonly &copy; @endhtmlonly 2022 Enrico Rossi <e.rossi@tecnobrain.com>
 *
 * @version 1.0.0
 *
 * @date 09 December 2022
 *
 * Based on a maple mini board.
 *
 * LGPL License Terms @ref lgpl_license
 */

/* Copyright (C) 2022 Enrico Rossi <e.rossi@tecnobrain.com>
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <errno.h>

#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/nvic.h>

enum { CH5, CH6, CH7 };

// Static allocations
const uint8_t channel_lenght { 3 };
uint16_t channels[channel_lenght];
constexpr uint8_t chanlist[channel_lenght] { 5, 6, 7 };
volatile bool flag;

// ISR
void dma1_channel1_isr()
{
	flag = true;

	if (dma_get_interrupt_flag(DMA1, DMA_CHANNEL1, DMA_TCIF))
		dma_clear_interrupt_flags(DMA1, DMA_CHANNEL1, DMA_TCIF);
}

void clock_setup()
{
	// high-speed external oscillator (HSE) at 8MHz.
	rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
	rcc_periph_clock_enable(RCC_GPIOA); // PA6
	rcc_periph_clock_enable(RCC_GPIOB); // PB1 Led
	rcc_periph_clock_enable(RCC_USART1);
}

void gpio_setup()
{
	// Led
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ,
			GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);

	// USART TX
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
			GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);
}

void usart_setup()
{
	/* Setup UART parameters. */
	usart_set_baudrate(USART1, 9600);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_mode(USART1, USART_MODE_TX);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

	/* Finally enable the USART. */
	usart_enable(USART1);
}

void usart_print(const char *s)
{
	while (*s)
		usart_send_blocking(USART1, (uint16_t)*s++);
}

/** @brief setup the DMA.
 *
 * We are going to use the DMA1 channel 1.
 *
 */
void dma_setup()
{
	// enable IRQ
	// Without this the timer interrupt routine will never be called.
	nvic_enable_irq(NVIC_DMA1_CHANNEL1_IRQ);

	// configure the DMA
	rcc_periph_clock_enable(RCC_DMA1);

	// The channel is disabled and configuration registers are cleared.
	dma_channel_reset(DMA1, DMA_CHANNEL1);

	// Priority.
	//dma_set_priority(DMA1, DMA_CHANNEL1, DMA_CCR_PL_VERY_HIGH);
	dma_set_priority(DMA1, DMA_CHANNEL1, DMA_CCR_PL_LOW);

	/* 16Bit wide transfer for source and destination. */
	// dma_set_memory_size(DMA1, DMA_CHANNEL1, DMA_CCR_MSIZE_32BIT);
	// dma_set_peripheral_size(DMA1, DMA_CHANNEL1, DMA_CCR_PSIZE_32BIT);
	dma_set_memory_size(DMA1, DMA_CHANNEL1, DMA_CCR_MSIZE_16BIT);
	dma_set_peripheral_size(DMA1, DMA_CHANNEL1, DMA_CCR_PSIZE_16BIT);

	/*
	 * After every 32bits we have to increase the address because
	 * we use RAM.
	 */
	// dma_enable_peripheral_increment_mode(DMA1, DMA_CHANNEL1);
	//dma_disable_peripheral_increment_mode(DMA1, DMA_CHANNEL1); // should be default
	dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL1);
	dma_enable_circular_mode(DMA1, DMA_CHANNEL1);

	/* We define the source as peripheral. */
	dma_set_read_from_peripheral(DMA1, DMA_CHANNEL1);

	/* We want to transfer ADC Data Register. */
	dma_set_peripheral_address(DMA1, DMA_CHANNEL1, (uint32_t)&ADC1_DR);

	/* Destination should be the channels array. */
	dma_set_memory_address(DMA1, DMA_CHANNEL1, (uint32_t)channels);

	/*
	 * Set number of DATA to transfer.
	 * Remember that this means not necessary bytes but MSIZE or PSIZE
	 * depending on your source device.
	 */
	dma_set_number_of_data(DMA1, DMA_CHANNEL1, channel_lenght);

	dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL1);

	/* Start DMA transfer. */
	dma_enable_channel(DMA1, DMA_CHANNEL1);
}

/**
 * Initialize the ADC1.
 *
 * @warning: rcc_periph_clock_enable(RCC_GPIOA);
 */
void adc_setup()
{
	gpio_set_mode(GPIOA,
			GPIO_MODE_INPUT,
			GPIO_CNF_INPUT_ANALOG,
			GPIO5 | GPIO6 | GPIO7);

	rcc_periph_clock_enable(RCC_ADC1);
	adc_power_off(ADC1); // Make sure the ADC doesn't run during config.

	dma_setup();

	/* rcc_peripheral_reset + rcc_peripheral_clear_reset */
	rcc_periph_reset_pulse(RST_ADC1);

	/* Prescaler */
	rcc_set_adcpre(RCC_CFGR_ADCPRE_PCLK2_DIV6);

	/* Independent mode */
	// adc_set_dual_mode(ADC_CR1_DUALMOD_IND);

	/* Scan mode */
	// adc_disable_scan_mode(ADC1); // convert only 1 channel
	adc_enable_scan_mode(ADC1); // scan channels mode (DMA required)

	/* configure for one single conversion. */
	adc_set_single_conversion_mode(ADC1);

	// F1 software trigger mandatory
	adc_enable_external_trigger_regular(ADC1, ADC_CR2_EXTSEL_SWSTART);
	// adc_disable_external_trigger_regular(ADC1);

	/* alignment */
	adc_set_right_aligned(ADC1);

	// adc_set_sample_time(ADC1, ADC_CHANNEL6, ADC_SMPR_SMP_239DOT5CYC);
	adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_239DOT5CYC);

	adc_set_regular_sequence(ADC1, channel_lenght, (uint8_t*)chanlist);

	adc_enable_dma(ADC1);
	adc_power_on(ADC1);
	adc_reset_calibration(ADC1);
	adc_calibrate(ADC1);
}

int main() {
	const uint8_t bsize { 255 };
	char buffer[bsize]; // generic buffer

	clock_setup();
	gpio_setup();
	usart_setup();
	adc_setup();

	usart_print("Test Maple ADC and DMA.\r\n");

	// Toggle the LED on and off forever
	while (1) {
		gpio_toggle(GPIOB, GPIO1); // LED on/off
		flag = false;

		adc_start_conversion_regular(ADC1);

		while (!flag); // Wait until DMA IRQ.

		sprintf(buffer, "ADC1 [ch5, ch6, ch7]: %4d, %4d, %4d\n",
				channels[CH5], channels[CH6], channels[CH7]);
		usart_print(buffer);

		for (long int i = 0; i < 1000000; i++)	// Wait a bit.
			__asm__("nop");
	}

	return 0;
}
