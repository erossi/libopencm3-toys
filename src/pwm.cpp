/* Copyright (C) 2021 Enrico Rossi
 * GPL 3

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*! \file
 *
 * Generates a constant pwm signal.
 * On a Maple Mini
 */

#include <stdio.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>

#include "tick.h"

static void clock_setup(void)
{
	// high-speed external oscillator (HSE) at 8MHz.
	rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);

	// PA0 Tim2CH1
	rcc_periph_clock_enable(RCC_GPIOA);

	// PB0 Led
	rcc_periph_clock_enable(RCC_GPIOB);

	// Clock Enable TIMer2
	rcc_periph_clock_enable(RCC_TIM2);
}

static void gpio_setup(void)
{
	// Led PB1
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ,
			GPIO_CNF_OUTPUT_PUSHPULL, GPIO1);

	// Enable GPIO A-1 to alternate func. TIMer2 CHannel 1.
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ,
			GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM2_CH1_ETR);

  // Enable GPIO A-8 output MCO 50Mhz
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO8);
}

static void pwm_suspend()
{
	// Stop
	timer_disable_counter(TIM2);

	// Clear
	timer_set_counter(TIM2, 0);

	// Enable the output channel
	timer_disable_oc_output(TIM2, TIM_OC1);
}

static void pwm_resume()
{
	// Enable the output channel
	timer_enable_oc_output(TIM2, TIM_OC1);

	// The the compare value for the channel 1
	// fixed to ~50% (half of the ARR).
	timer_set_oc_value(TIM2, TIM_OC1, 128);

	// Start
	timer_enable_counter(TIM2);
}

/*! PWM timer setup
 *
 * note: Fpwm = Fclk/((ARR + 1) * (PSC + 1))
 * where ARR is the period and PSC is the prescaler.
 *
 * 72000÷((256+1) × (8+1)) = 31.128 Khz
 */
static void pwm_setup()
{
	// Reset TIM2 peripheral (old timer_reset()).
	rcc_periph_reset_pulse(RST_TIM2);

	// Set the clock, edge, dir up.
	timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);

	// Set the prescaler PSC
	timer_set_prescaler(TIM2, 8);

	// Set the ARR
	timer_set_period(TIM2, 256);

	// Compare mode to PWM and enable the output channel 1.
	// Timer 2, Output Channel 1,
	// PWM2 - The output is inactive when the counter is
	// less than the compare register contents and active otherwise.
	timer_set_oc_mode(TIM2, TIM_OC1, TIM_OCM_PWM2);

	// Suspend
	pwm_suspend();
}

void _delay_ms(const uint32_t ms)
{
  const uint64_t i { _millis + ms };

  while (_millis < i )
    asm("nop");
}

int main() {
	clock_setup();
	systick_setup();
	gpio_setup();
	pwm_setup();
	pwm_resume();

  // Output MCO
  rcc_set_mco(RCC_CFGR_MCO_HSE);

	// Toggle the LED on and off forever
	while (1) {
		gpio_toggle(GPIOB, GPIO1);	/* LED on/off */
		_delay_ms(1000);
	}

	return 0;
}
